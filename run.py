#! /usr/bin/env python3

import sys
import subprocess
import re

def is_num(str):
    try:
        float(str)
        return True
    except ValueError:
        return False

def get_tests():
    """ Gather all test cases from sixonix using list option. """
    raw_out = subprocess.check_output('./sixonix.py list', shell=True)
    out = raw_out.decode('utf-8').strip()
    tests = [t.strip().lstrip() for t in out.split('\n')]
    tests.sort()
    return tests
   
def get_test_groups(tests):
    """ Gather test groups by identifying unique prefixes of test cases """
    test_groups = [t.split('.')[0] for t in tests]
    test_groups = list(set(test_groups))
    test_groups.sort()
    return test_groups

def filter_tests_by_name(tests, filter):
    """ Apply filter to list of tests and return matches. Filters supported: 
        * match all that begin with filter
        * 'all' is keyword for selecting all tests
        * ',' separates multiple additive filters
        * '-' filter prefix means remove from list
        * '*' supports wildcards
    """
    # 'all' is a keyword to run everything
    if filter == '' or filter == 'all':
        return [tests]

    filtered_tests = []
    for f in filter.split(','):
        # '-' prefix indicates removing matching tests from list
        subtract_filter = f.startswith('-')
        if subtract_filter:
            f = f[1:]
            
        if '*' in f:
            # Handle wildcard search
            f = f.replace('*','.*')
            matching_tests = [t for t in tests if re.fullmatch(f, t)]
        else:
            matching_tests = [t for t in tests if t.startswith(f)]

        # Just in case one test name is a subset of another.
        # Run 1 test if single match exactly (e.g. .fill and .fill_off)
        if f in matching_tests:
            matching_tests += [f]
        
        if subtract_filter:
            filtered_tests = [t for t in filtered_tests if t not in matching_tests]
        else:
            filtered_tests += matching_tests
        
    # Remove duplicates
    filtered_tests = list(set(filtered_tests))
    filtered_tests.sort()
    return filtered_tests

def sixonix_list(args):
    tests = get_tests()

    if len(args) == 0:
        # List all test group names and # test cases per group
        for group in get_test_groups(tests):
            tests_in_group = [t for t in tests if t.startswith(group)]
            print('%-20s  (%2d tests)' % (group, len(tests_in_group)))
    else:
        # List all test cases in test group
        test_filter = args[0]
        test_cases_in_group = filter_tests_by_name(tests, test_filter)
        if len(test_cases_in_group) == 0:
            print('Unknown test group "%s".' % test_filter)
            sys.exit(1)
        print('\n'.join(test_cases_in_group))
    print('')

def sixonix_run(args):
    test_filter = args[0]
    tests_to_run = filter_tests_by_name(get_tests(), test_filter)
    for test in tests_to_run:
        print('%-40s' % (test + ', '), end='', flush=True)
        raw_out = subprocess.check_output('./sixonix.py run %s' % test, shell=True)
        out = raw_out.decode('utf-8').strip().lstrip()
        if (is_num(out)):
            print('%3.2f' % float(out))
        else:
            print (out)

def main(main_args=sys.argv[1:]):
    if main_args[0] == 'list':
        sixonix_list(main_args[1:])
    elif main_args[0] == 'run':
        sixonix_run(main_args[1:])
    else:
        print('Usage: run.py [list|run] *')

if __name__ == '__main__':
    sys.exit(main())